#include "Nucleus.h"

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_strand_dna_complementary_on = on_complementary_dna_strand;
}

unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_strand_dna_complementary_on;
}

void Gene::setStart(const unsigned int newStart)
{
	this->_start = newStart;
}

void Gene::setEnd(const unsigned int newEnd)
{
	this->_end = newEnd;
}

void Gene::setStrand(const bool newStrandSet)
{
	this->_strand_dna_complementary_on = newStrandSet;
}

void Nucleus::init(const std::string dna_sequence)
{
	if (dna_sequence.find_first_not_of("ACTG") != std::string::npos) // if a char in the string is not ACTG
	{
		std::cerr << "The dna_sequence must include only the letters A, C, T, G"; // therow error
		_exit(1);
	}
	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = "" + _DNA_strand;
	std::string& s = this->_complementary_DNA_strand; // for shorter name
	char r = 0; //replacement
	std::map<char, char> rs = { {'G', 'C'}, {'C', 'G'}, {'A', 'T'},  {'T', 'A'} }; // found this cool trick thanks to stackoverflow
	std::replace_if(s.begin(), s.end(), [&](char c) { return r = rs[c]; }, r); // replaces all of the letters to their corresponding letter
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string strandToSlice = "", RNA_transcript = "";
	if (gene.is_on_complementary_dna_strand()) // determine the strand
	{
		strandToSlice = this->_complementary_DNA_strand;
	}
	else
	{
		strandToSlice = this->_DNA_strand;
	}
	RNA_transcript = strandToSlice.substr(gene.get_start(), gene.get_end() - gene.get_start()); // slice a part of the strand
	std::replace(RNA_transcript.begin(), RNA_transcript.end(), 'T', 'U'); // replace all T with U
	return RNA_transcript;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string copy(this->_DNA_strand);
	std::reverse(copy.begin(), copy.end());
	return copy;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int occurrences = 0;
	std::string::size_type pos = 0;
	while ((pos = this->_DNA_strand.find(codon, pos)) != std::string::npos) {
		++occurrences;
		pos += codon.length();
	}
	return occurrences;
}


