#include "Mitochondrion.h"

void Mitochondrion::init()
{
	this->_glucose_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcid desirable_protein[SIZE_OF_DESIRABLE_OUTCOME] = DESIRABLE_OUTCOME;
	AminoAcidNode * curr = protein.get_first();
	int i = 0, flag = 1; // flag is for if the protein recived is the desirable one
	while (curr)
	{
		if (curr->get_data() != desirable_protein[i])
		{
			flag = 0;
		}
		curr = curr->get_next();
		i++;
	}
	if (flag)
	{
		this->_has_glocuse_receptor = true;
	}
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glucose_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	return this->_has_glocuse_receptor && this->_glucose_level >= MINIUM_GLUCOSE_LEVEL;
}
