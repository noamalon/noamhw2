#pragma once
#include <string>
#include <algorithm>
#include <map>
#include <iostream>

class Gene
{
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	//getters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;
	//setters
	void setStart(const unsigned int newStart);
	void setEnd(const unsigned int newEnd);
	void setStrand(const bool newStrandSet);
private:
	unsigned int _start;
	unsigned int _end;
	bool _strand_dna_complementary_on;
};

class Nucleus
{
public:
	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
private:
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;
};


