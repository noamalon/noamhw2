#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;
}

bool Cell::get_ATP()
{
	int flag = 0;
	std::string gene_transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	Protein * glucose_receptor_protein = this->_ribosome.create_protein(gene_transcript);
	if (!glucose_receptor_protein)
	{
		std::cerr << "Couldn't make protein"; // throw error
		_exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*glucose_receptor_protein);
	this->_mitochondrion.set_glucose(MINIUM_GLUCOSE_LEVEL);
	if (this->_mitochondrion.produceATP())
	{
		flag = 1;
		this->_atp_units = 100;
	}
	return flag;
}
