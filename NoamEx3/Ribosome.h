#pragma once
#include "Protein.h"
#define NUCLEOTIDES_PER_ACID 3

class Ribosome
{
public:
	Protein* create_protein(std::string& RNA_transcript) const;
};

