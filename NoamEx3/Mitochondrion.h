#pragma once
#include "Protein.h"
#define DESIRABLE_OUTCOME {ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END}
#define SIZE_OF_DESIRABLE_OUTCOME 7
#define MINIUM_GLUCOSE_LEVEL 50
class Mitochondrion
{
public:
	void init();
	void insert_glucose_receptor(const Protein& protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;
private:
	unsigned int _glucose_level;
	bool _has_glocuse_receptor;
};

