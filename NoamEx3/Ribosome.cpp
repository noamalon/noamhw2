#include "Ribosome.h"

Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	std::string acidCode = "";
	Protein * newProtein = new Protein;
	AminoAcidNode * curr = new AminoAcidNode;
	newProtein->set_first(curr);
	while (RNA_transcript.length() >= 3)
	{
		acidCode = RNA_transcript.substr(0, NUCLEOTIDES_PER_ACID); // take acidCode 
		RNA_transcript.erase(0, NUCLEOTIDES_PER_ACID); //erase the taken acidCode
		if (UNKNOWN == get_amino_acid(acidCode))
		{
			RNA_transcript = "";
			newProtein->clear();
			delete newProtein;
			newProtein = nullptr;
		}
		else
		{
			curr->set_data(get_amino_acid(acidCode));
			curr->set_next(new AminoAcidNode);
			curr = curr->get_next();
		}
	}
	curr->set_data(AMINO_CHAIN_END);
	curr->set_next(nullptr);
	return newProtein;
}
